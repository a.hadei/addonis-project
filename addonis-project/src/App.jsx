import Footer from "./components/Footer/Footer";
import NavBar from "./components/NavBar/NavBar";
import Header from "./components/header/Header";
import Body from "./views/Body/Body";
import Home from "./views/Home/Home";
import { Routes, Route, Navigate } from "react-router-dom";
import ContactUs from "./views/ContactUs/ContactUs";
import About from "./views/About/About";
import Register from "./views/Register/Register";
import SignIn from "./views/SignIn/SignIn";
import EditProfile from "./views/EditProfile/EditProfile";
import AdminPanel from "./views/AdminPanel/AdminPanel";
import { useState, useEffect, } from 'react'
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './firebase-config/firebase-config';
import { getUserDataByUID } from "./services/users.services";
import { AuthContext } from "./context/AuthContext";
import UploadAddon from "../src/components/uploadAddon/UploadAddon";
import AdminDashboard from "./views/AdminDashboard/AdminDashboard";
import { get, set, ref, push, update, serverTimestamp, } from "firebase/database";
import { db } from "../src/firebase-config/firebase-config";
import CustomModal from '../src/components/CustomModal/CustomModal'
import UserUploadedAddons from "./views/UserUploadedAddons/UserUploadedAddons";
import "./App.css";




function App() {
  const [user] = useAuthState(auth);
  const [addons, setAddons] = useState([]);
  const [appState, setAppState] = useState({
    user,
    userData: null,
  });

  useEffect(() => {

    const addonsRef = ref(db, "addons");
    get(addonsRef).then((snapshot) => {
      if (snapshot.exists()) {
        const addonData = snapshot.val();
        const addonArray = Object.values(addonData);
        setAddons(addonArray);
      }
    });
  }, []);

  useEffect(() => {
    if (user === null) {
      setAppState({ user: null, userData: null });
      return;
    }

    getUserDataByUID(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('User data not found');
        }

        const userData = snapshot.val(Object.keys(snapshot.val())[0]);
        setAppState({
          user,
          userData,
        });
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
      });
  }, [user]);

  const isAdmin = appState.userData?.role === 'admin';

  const [showBlockedModal, setShowBlockedModal] = useState(false);
  const [blockedMessage, setBlockedMessage] = useState('');

  useEffect(() => {
    if (appState.userData?.blocked) {
      setBlockedMessage('Your account is currently blocked. Please contact support for assistance.');
      setShowBlockedModal(true);

      setTimeout(() => {
        setShowBlockedModal(false);
        setBlockedMessage('');
      }, 5000);
    }
  }, [appState.userData?.blocked]);

  return (
    <>
      <AuthContext.Provider value={{ ...appState, setUser: setAppState }}>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contactus" element={<ContactUs />} />
          {user === null && <Route path="/register" element={<Register />} />}
          {user === null && <Route path="/signin" element={<SignIn />} />}
          <Route path="/editprofile" element={<EditProfile />} />
          <Route path="/adminpanel" element={isAdmin ? <AdminPanel /> : <Navigate to="/" />} />
          <Route path="/uploadaddon" element={<UploadAddon />} />
          <Route path="/admindashboard" element={isAdmin ? <AdminDashboard /> : <Navigate to="/" />} />
          <Route path="/useruploadedaddons" element={<UserUploadedAddons />} />

        </Routes>
      </AuthContext.Provider>
      <Footer />
      {showBlockedModal && <CustomModal message={blockedMessage} />}
    </>
  );
}

export default App;