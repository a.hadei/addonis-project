import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";

export const firebaseConfig = {
  apiKey: "AIzaSyC2qSW9CfCDgbfhQ-AuLkjObh62NonSEHs",
  authDomain: "addonis-project-4e647.firebaseapp.com",
  databaseURL: "https://addonis-project-4e647-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "addonis-project-4e647",
  storageBucket: "addonis-project-4e647.appspot.com",
  messagingSenderId: "103800247001",
  appId: "1:103800247001:web:84ee1e57ed45d49ccae4c8",
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);