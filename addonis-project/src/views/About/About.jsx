import "./About.css";

export default function About() {
  return (
    <div className="about">
      <div className="container">
        <h2 className="mx-auto fs-3 my-5 fw-medium custom-title">About Us</h2>
        <br />
        <section>
          <h3>Our Story:</h3>
          <p>
            At Addonis Galaxy, we believe in the power of innovation and
            quality. Our journey began with a simple idea: to create a place
            where people can discover and enjoy the latest and greatest addons
            for their favorite applications.
          </p>
        </section>
        <section>
          <h3>Our Mission:</h3>
          <p>
            Our mission is to provide top-notch addons that enhance your
            digital experience. We're committed to offering the best products,
            backed by exceptional customer service.
          </p>
        </section>
        <section>
          <h3>What Sets Us Apart:</h3>
          <ul>
            <p>
              <strong>Quality:</strong> We carefully curate and test each addon
              to ensure it meets the highest standards.
            </p>
            <p>
              <strong>Innovation:</strong> We stay at the forefront of
              technology to bring you the latest and most innovative addons.
            </p>
            <p>
              <strong>Community:</strong> We value our community and strive to
              create a space where enthusiasts can connect and share their
              passion.
            </p>
          </ul>
        </section>
        <section>
          <h3>Meet Our Team:</h3>
          <br />
          <div className="team-member">
            <h4>Teodosi Ivanov</h4>
            <p>
              Teodosi is our lead developer and a tech enthusiast. With over 2 months
              of experience in software development, he's the driving force behind our
              innovative addons.
            </p>
          </div>
          <br />
          <div className="team-member">
            <h4>Anushirvan Hadei</h4>
            <p>
              Anushirvan is our design guru. He's responsible for creating sleek and
              user-friendly interfaces for our addons, making them a joy to use.
            </p>
          </div>
          <br />
          <div className="team-member">
            <h4>Kiril Marinov</h4>
            <p>
              Kiril is our customer support hero. He's always ready to assist you with
              any questions or issues you might have. Your satisfaction is his
              priority.
            </p>
          </div>
        </section>
      </div>
    </div>
  );
}