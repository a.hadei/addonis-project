import { useContext, useState } from "react";
import { AuthContext } from "../../context/AuthContext";
import { reauthenticateWithCredential, EmailAuthProvider, updateEmail } from "firebase/auth";
import {
  updateFirstNameInFirebase,
  updateLastNameInFirebase,
  updateEmailInFirebase,
  updateProfilePictureInFirebase
} from "../../services/editUser";
import { uploadBytes, ref, getDownloadURL } from "firebase/storage";
import { storage } from "../../firebase-config/firebase-config";
import { Form, Button, Alert } from "react-bootstrap";
import "./EditProfile.css";


export default function EditProfile() {
  const { user, userData, setUser } = useContext(AuthContext);
  const [newEmail, setNewEmail] = useState("");
  const [newFirstName, setNewFirstName] = useState("");
  const [newLastName, setNewLastName] = useState("");
  const [currentPassword, setCurrentPassword] = useState("");
  const [image, setImage] = useState(null);
  const [message, setMessage] = useState("");
  const [messageType, setMessageType] = useState("");

  const clearMessage = () => {
    setMessage("");
    setMessageType("");
  };

  if (!user) {
    return <div>User not logged in.</div>;
  }

  const currentEmail = user.email || "";
  const currentFirstName = userData?.firstName || "";
  const currentLastName = userData?.lastName || "";
  const currentProfilePicture = userData?.profilePictureUrl || "";

  const handleUpdate = async (updateFunction, newValue, stateSetter, fieldName) => {
    try {
      await updateFunction(user.uid, newValue);

      setUser((prevUser) => ({
        ...prevUser,
        userData: {
          ...prevUser.userData,
          [fieldName]: newValue
        }
      }));

      stateSetter("");
    } catch (error) {
      setMessage("Error updating: " + error.message);
      setMessageType("error");
    }
  };

  const handleUpdateEmail = async () => {
    try {
      const credentials = EmailAuthProvider.credential(currentEmail, currentPassword);
      await reauthenticateWithCredential(user, credentials);

      await updateEmail(user, newEmail);

      await updateEmailInFirebase(user.uid, newEmail);

      setUser((prevUser) => ({
        ...prevUser,
        userData: {
          ...prevUser.userData,
          email: newEmail
        }
      }));

      setNewEmail("");
      setCurrentPassword("");
    } catch (error) {
      setMessage("Error updating email: " + error.message);
      setMessageType("error");
    }
  };

  const handleImageChange = (e) => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };

  const handleSubmitImage = () => {
    if (!image) {
      setMessage("Please select an image to upload.");
      setMessageType("error");
      return;
    }

    const imageRef = ref(storage, `users/${user.uid}/profilePicture/${image.name}`);
    uploadBytes(imageRef, image)
      .then(() => {
        getDownloadURL(imageRef).then((url) => {
          updateProfilePictureInFirebase(user.uid, url)
            .then(() => {
              setUser((prevUser) => ({
                ...prevUser,
                userData: {
                  ...prevUser.userData,
                  profilePictureUrl: url
                }
              }));
              setMessage("Profile picture uploaded successfully!");
              setMessageType("success");
            })
            .catch((error) => {
              setMessage("Error updating profile picture URL: " + error.message);
              setMessageType("error");
            });
        })
        .catch((error) => {
          setMessage("Error getting the image URL: " + error.message);
          setMessageType("error");
        });
        setImage(null);
      })
      .catch((error) => {
        setMessage("Error uploading image: " + error.message);
        setMessageType("error");
      });
  };

  const handleSaveChanges = async () => {
    clearMessage();

    try {
      if (newFirstName) {
        await handleUpdate(updateFirstNameInFirebase, newFirstName, setNewFirstName, "firstName");
      }

      if (newLastName) {
        await handleUpdate(updateLastNameInFirebase, newLastName, setNewLastName, "lastName");
      }

      if (newEmail && currentPassword) {
        const credentials = EmailAuthProvider.credential(currentEmail, currentPassword);
        await reauthenticateWithCredential(user, credentials);
        await updateEmail(user, newEmail);
        await updateEmailInFirebase(user.uid, newEmail);
        setUser((prevUser) => ({
          ...prevUser,
          userData: {
            ...prevUser.userData,
            email: newEmail
          }
        }));
        setNewEmail("");
        setCurrentPassword("");
      }

      if (image) {
        const imageRef = ref(storage, `users/${user.uid}/profilePicture/${image.name}`);
        await uploadBytes(imageRef, image);
        const url = await getDownloadURL(imageRef);
        await updateProfilePictureInFirebase(user.uid, url);
        setUser((prevUser) => ({
          ...prevUser,
          userData: {
            ...prevUser.userData,
            profilePictureUrl: url
          }
        }));
        setMessage("Profile updates saved successfully!");
        setMessageType("success");
      }
    } catch (error) {
      setMessage("Error updating profile: " + error.message);
      setMessageType("error");
    }
  };

  return (
    <div className="container mt-4">
      <p className="mx-auto fs-3 my-5 fw-medium custom-title">Edit Profile</p>
      <div className="col-md-6 mx-auto">
        {message && (
          <Alert variant={messageType === "error" ? "danger" : "success"} onClose={clearMessage} dismissible>
            {message}
          </Alert>
        )}
        <Form>
          <Form.Group className="mb-3">
            <Form.Label>Your current first name is:</Form.Label>
            <Form.Control type="text" value={currentFirstName} readOnly />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Your current last name is:</Form.Label>
            <Form.Control type="text" value={currentLastName} readOnly />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Your current email is:</Form.Label>
            <Form.Control type="email" value={currentEmail} readOnly />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Write your new first name:</Form.Label>
            <Form.Control
              type="text"
              value={newFirstName}
              onChange={(e) => setNewFirstName(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Write your new last name:</Form.Label>
            <Form.Control
              type="text"
              value={newLastName}
              onChange={(e) => setNewLastName(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Write your new email:</Form.Label>
            <Form.Control
              type="text"
              value={newEmail}
              onChange={(e) => setNewEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Enter your current password:</Form.Label>
            <Form.Control
              type="password"
              value={currentPassword}
              onChange={(e) => setCurrentPassword(e.target.value)}
            />
          </Form.Group>
        </Form>
        <div className="mb-3">
          <label>Upload a new profile picture:</label>
          <input type="file" onChange={handleImageChange} className="form-control" />
        </div>
        <div className="profilePicture">
          <Form.Label className="custom-content">Current Profile Picture:</Form.Label>
          <img src={currentProfilePicture} alt="Profile" className="d-block mx-auto mb-3 custom-profile-picture" />
          <Button onClick={handleSaveChanges} className="custom-button">Save Changes</Button>
        </div>
      </div>
    </div>
  );
}