import AvatarItem from '../../components/AvatarItem/AvatarItem';
import Stack from 'react-bootstrap/Stack';
import "./ContactUs.css";


export default function Contactus () {
    return (
        <div className='contactus'>
            <p className="mx-auto fs-3 my-5 fw-medium custom-title">Contact Us</p>
            <Stack className="col-md-3 mx-auto mt-5 custom-content" gap={4}>
                <AvatarItem name="Teodosi Ivanov" email="teodosi.ivanov.a49@learn.telerikacademy.com" imageUrl="src/icons/1.jpeg" />
                <AvatarItem name="Anushirvan Hadei" email="anushirvan.hadei.a49@learn.telerikacademy.com" imageUrl="src/icons/1.jpeg" />
                <AvatarItem name="Kiril Marinov" email="kiril.marinov.a49@learn.telerikacademy.com" imageUrl="src/icons/1.jpeg" />
            </Stack>
        </div>
    )
}