import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { onValue, ref } from "firebase/database";
import { db } from "../../firebase-config/firebase-config";
import { checkIfUserExists, registerUser, checkIfPhoneExists } from "../../services/auth.services";
import { checkUsernameAvailability } from "../../services/auth.services";
import { createUser } from "../../services/users.services";
import { AuthContext } from "../../context/AuthContext";
import {
  MIN_FIRSTNAME_LENGTH,
  MAX_FIRSTNAME_LENGTH,
  MIN_LASTNAME_LENGTH,
  MAX_LASTNAME_LENGTH,
  MIN_USERNAME_LENGTH,
  MAX_USERNAME_LENGTH,
  MIN_PASSWORD_LENGTH,
  MAX_PASSWORD_LENGTH,
  MIN_PHONE_NUMBER_LENGTH,
  ADMIN_EMAIL,
} from "../../constants/constants";
import Select from "react-select";
import { countryCodes } from "../../constants/countryCodes"
import { Button, Form } from "react-bootstrap";
import SWModal from "./SWModal"
import "./Register.css";


export default function Register() {
  const [form, setForm] = useState({
    email: "",
    password: "",
    username: "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
  });

  const [selectedCountryCode, setSelectedCountryCode] = useState(null);
  const [countryCodeError, setCountryCodeError] = useState("");


  const [showModal, setShowModal] = useState(false); 

  const handleCountryCodeChange = (selectedOption) => {
    setSelectedCountryCode(selectedOption);

    if (selectedOption && selectedOption.value === "+66") {
      setShowModal(true); 
    } else {
      setShowModal(false); 
    }
  };

  const { setUser } = useContext(AuthContext);
  const navigate = useNavigate();

  const [showRegisterMessage, setShowRegisterMessage] = useState(false);
  const [registerMessage, setRegisterMessage] = useState({
    message: "",
    variant: "",
  });

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const onRegister = async () => {
    try {
      if (!form.email) {
        showError("Email is required!");
        return;
      }
      if (
        !form.password ||
        form.password.length < MIN_PASSWORD_LENGTH ||
        form.password.length > MAX_PASSWORD_LENGTH
      ) {
        showError(
          "Password is required and it must be between 6 and 128 characters."
        );
        return;
      }
      if (
        !form.username ||
        form.username.length < MIN_USERNAME_LENGTH ||
        form.username.length > MAX_USERNAME_LENGTH
      ) {
        showError(
          "Username is required and it must be between 4 and 32 characters."
        );
        return;
      }
      if (
        !form.firstName ||
        form.firstName.length < MIN_FIRSTNAME_LENGTH ||
        form.firstName.length > MAX_FIRSTNAME_LENGTH
      ) {
        showError("First name must be between 4 and 32 characters.");
        return;
      }
      if (
        !form.lastName ||
        form.lastName.length < MIN_LASTNAME_LENGTH ||
        form.lastName.length > MAX_LASTNAME_LENGTH
      ) {
        showError("Last name must be between 4 and 32 characters.");
        return;
      }
      if (
        !form.phoneNumber ||
        form.phoneNumber.length < MIN_PHONE_NUMBER_LENGTH
      ) {
        showError("Phone number must be at least six digits long.");
        return;
      }

      if (!selectedCountryCode) {
        setCountryCodeError("Please select a country code.");
        return;
      }

      const phoneExists = await checkIfPhoneExists(
        `${selectedCountryCode.value} ${form.phoneNumber}`
      );

      if (phoneExists) {
        showError("Phone number is already registered. Please use a different phone number.");
        return;
      }
      const exists = await checkIfUserExists(form.email);
      if (exists) {
        showError("User with this email already exists!");
        return;
      }

      console.log("Checking username availability...");
      const isUsernameAvailable = await checkUsernameAvailability(form.username);
      console.log("Username available:", isUsernameAvailable);
      if (!isUsernameAvailable) {
        showError(
          "Username is already taken. Please choose a different username."
        );
        return;
      }

      const role = form.email === ADMIN_EMAIL ? "admin" : "user";

      const credential = await registerUser(form.email, form.password);
      const { uid } = credential.user;

      await createUser(
        form.username,
        uid,
        form.email,
        form.firstName,
        form.lastName,
        `${selectedCountryCode?.value || ""} ${form.phoneNumber}`,
        role
      );

      const userRef = ref(db, `users/${uid}`);
      onValue(userRef, (snapshot) => {
        const userData = snapshot.val();
        if (userData) {
          setUser({
            user: {
              ...credential.user,
              firstName: form.firstName,
              lastName: form.lastName,
              role,
            },
          });
        }
      });

      navigate("/");
    } catch (error) {
      console.error("Error during registration:", error);
    } finally {
      setShowRegisterMessage(true);
      setTimeout(() => {
        setShowRegisterMessage(false);
        setRegisterMessage({ message: "", variant: "" });
      }, 3000);
    }
  };

  const showError = (message) => {
    setRegisterMessage({ message, variant: "danger" });
    setShowRegisterMessage(true);
  };

  return (
    <>
      <div className="register">
      <p className="mx-auto fs-3 my-5 fw-medium custom-title">Register</p>
        <Form>
          <div className="col-md-4 mx-auto">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label className="custom-content">Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={form.email}
                onChange={updateForm("email")}
                className="form-control-custom"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formUsername">
              <Form.Label className="custom-content">Username</Form.Label>
              <Form.Control
                placeholder="Username"
                value={form.username}
                onChange={updateForm("username")}
                className="form-control-custom"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstName">
              <Form.Label className="custom-content">First Name</Form.Label>
              <Form.Control
                placeholder="First Name"
                value={form.firstName}
                onChange={updateForm("firstName")}
                className="form-control-custom"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formLastName">
              <Form.Label className="custom-content">Last Name</Form.Label>
              <Form.Control
                placeholder="Last Name"
                value={form.lastName}
                onChange={updateForm("lastName")}
                className="form-control-custom"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label className="custom-content">Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={form.password}
                onChange={updateForm("password")}
                className="form-control-custom"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formPhoneNumber">
              <Form.Label className="custom-content">Phone Number</Form.Label>
              <div className="input-group">
                <Select
                  className="country-code-select"
                  placeholder="Select Country Code"
                  value={selectedCountryCode}
                  options={countryCodes}
                  onChange={handleCountryCodeChange}
                />
                <Form.Control
                  type="text"
                  placeholder="Phone Number"
                  value={form.phoneNumber}
                  onChange={updateForm("phoneNumber")}
                  className="form-control-custom"
                />
              </div>
              {countryCodeError && (
                <div className="text-danger">{countryCodeError}</div>
              )}
            </Form.Group>
            <Button
              className="w-100 my-4 custom-button"
              onClick={onRegister}
            >
              Register
            </Button>
            {showRegisterMessage && (
              <div className={`message ${registerMessage.variant}`}>
                {registerMessage.message}
              </div>
            )}
          </div>
        </Form>
      </div>
      {showModal && (
        <SWModal onClose={() => setShowModal(false)} />
      )}
    </>
  );
}
      