import "./SWModal.css"

export default function CustomModal(props) {
    const { onClose } = props;
  
   return (
    <div className="custom-modal-container">
      <div className="custom-modal-content">
        <div className="saber"></div>
        <div className="more-text-container">
          <div className="more-text">A valid country code you must pick</div>
        </div>
        <button className="close-button" onClick={onClose}>
          Close
        </button>
      </div>
    </div>
  );
}