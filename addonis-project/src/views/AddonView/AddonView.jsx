import { useState, useEffect } from "react";
import { db } from '../../firebase-config/firebase-config';
import { ref, onValue } from 'firebase/database';
import AddonDetails from "../../components/AddonDetails/AddonDetails";
import { Row, Col } from "react-bootstrap";
import "./AddonView.css"

const AddonList = () => {
  const [filterText, setFilterText] = useState("");
  const [sortBy, setSortBy] = useState("date");
  const [currentPage, setCurrentPage] = useState(1);
  const addonsPerPage = 10;

  const [addons, setAddons] = useState([]);

  const addonsRef = ref(db, 'addons');

  useEffect(() => {
    const unsubscribe = onValue(addonsRef, (snapshot) => {
      if (snapshot.exists()) {
        const addonData = snapshot.val();
        const addonList = Object.entries(addonData).map(([key, value]) => ({
          key,
          ...value,
        }));
        setAddons(addonList);
      } else {
        setAddons([]);
      }
    });


    return () => unsubscribe();
  }, []);

  const filteredAddons = addons
    .filter(addon => addon.status === "approved")
    .filter((addon) =>
      addon.name.toLowerCase().includes(filterText.trim()) ||
      addon.creator.toLowerCase().includes(filterText.trim()) ||
      addon.tags.some(tag => tag.toLowerCase().includes(filterText.trim()))
    );

  filteredAddons.sort((a, b) => {
    if (sortBy === "name") {
      return a.name.localeCompare(b.name);
    } else if (sortBy === "creator") {
      return a.creator.localeCompare(b.creator);
    } else if (sortBy === "tags") {
      const tagsA = a.tags.join(", ");
      const tagsB = b.tags.join(", ");
      return tagsA.localeCompare(tagsB);
    }
    return b.timestamp - a.timestamp;
  });

  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  const currentAddons = filteredAddons.slice(indexOfFirstAddon, indexOfLastAddon);

  const buildContent = () => {
    if (currentAddons.length) {
      return (
        <Row>
          {currentAddons.map((addon, index) => (
            <Col md={6} lg={4} key={index} className="mb-4">
              <AddonDetails addon={addon} />
            </Col>
          ))}
        </Row>)
    }

    return (<p className="mt-5" style={{ minHeight: "20rem" }}>No results found.</p>)
  }

  return (
    <div className="container col-md-8z mx-auto">
      <h2 className="mb-3 custom-title">List of Addons:</h2>
      <div className="d-flex mb-3 flex-row ">
        <input
          type="text"
          className="form-control me-3"
          placeholder="Search..."
          value={filterText}
          onChange={(e) => {
            setCurrentPage(1)
            setFilterText(e.target.value.toLowerCase())
          }}
        />
        <span className="my-auto fw-bold" style={{ minWidth: "5rem" }}>Sort By:</span>
        <select
          className="form-select"
          style={{ width: "10rem" }}
          value={sortBy}
          onChange={(e) => setSortBy(e.target.value)}
        >
          <option value="date">Date</option>
          <option value="name">Name</option>
          <option value="creator">Creator</option>
          <option value="tags">Tags</option>
        </select>
      </div>
      {buildContent()}
      <nav>
        <ul className="pagination justify-content-center mb-3">
          {Array(Math.ceil(filteredAddons.length / addonsPerPage))
            .fill()
            .map((_, index) => (
              <li key={index} className={`page-item ${currentPage === index + 1 ? "active" : ""}`}>
                <button className="page-link" onClick={() => setCurrentPage(index + 1)}>
                  {index + 1}
                </button>
              </li>
            ))}
        </ul>
      </nav>
    </div>
  );
}

export default AddonList;