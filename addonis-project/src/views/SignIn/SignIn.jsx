import { AuthContext } from "../../context/AuthContext";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH } from "../../constants/constants";
import { loginUser } from "../../services/auth.services";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "./SignIn.css";


export default function SignIn() {
  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const { setUser } = useContext(AuthContext);
  const navigate = useNavigate();

  const [showLoginMessage, setShowLoginMessage] = useState(false);
  const [loginMessage, setLoginMessage] = useState({ message: '', variant: '' });

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const onLogin = async () => {
    try {
      if (!form.email) {
        showError('Email is required!');
        return;
      }
      if (!form.password) {
        showError('Password is required!');
        return;
      }
      if (form.password.length < MIN_PASSWORD_LENGTH || form.password.length > MAX_PASSWORD_LENGTH) {
        showError('Password length should be between 6 and 32 characters!');
        return;
      }

      const credential = await loginUser(form.email, form.password);
      setUser({
        user: credential.user,
      });
      navigate('/');
    } catch (error) {
      showError('Something went wrong during login.');
    }
  };

  const showError = (message) => {
    setLoginMessage({ message, variant: 'danger' });
    setShowLoginMessage(true);
    setTimeout(() => {
      setShowLoginMessage(false);
      setLoginMessage({ message: '', variant: '' });
    }, 3000);
  };

  return (
    <div className="sign-in-wrapper">
        <p className="mx-auto fs-3 my-5 fw-medium custom-title">Sign In</p>
        <div className="col-md-4 mx-auto">
     <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label className="custom-content">Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={form.email}
                onChange={updateForm("email")}
              />
            </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label className="custom-content">Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={form.password}
                onChange={updateForm("password")}
              />
            </Form.Group>
            <Button className="w-100 mt-3 custom-button" variant="primary" onClick={onLogin}>
              Submit
            </Button>
      {showLoginMessage && (<div className={`message ${loginMessage.variant}`}>{loginMessage.message}</div>)}
      </Form>
    </div>
    </div>
  );
}