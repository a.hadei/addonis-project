import { useState } from 'react';
import { db } from '../../firebase-config/firebase-config';
import { ref, get, query, orderByChild, equalTo } from 'firebase/database';
import { Form, Button, Alert } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './UserSearchForm.css';

export default function UserSearchForm({ onUserFound }) {
    const [searchUsername, setSearchUsername] = useState('');
    const [message, setMessage] = useState(null);

    const handleSearch = () => {
        setMessage(null);

        if (searchUsername.trim() === '') {
            setMessage('Please enter a username for search.');
            return;
        }

        const usersRef = ref(db, 'users');
        const usernameQuery = query(usersRef, orderByChild('username'), equalTo(searchUsername));

        get(usernameQuery)
            .then((snapshot) => {
                if (snapshot.exists()) {
                    const userData = snapshot.val();
                    const userKey = Object.keys(userData)[0];
                    const user = userData[userKey];
                    console.log('Retrieved user:', user);
                    onUserFound(user);
                } else {
                    setMessage('User not found.');
                }
            })
            .catch((error) => {
                console.error('Error searching for user:', error);
                setMessage('An error occurred while searching for the user. Please try again later.');
            });
    };

    return (
        <div className="centered-container">
            <Form>
                <Form.Group controlId="searchUsername">
                    <Form.Label>User's username</Form.Label>
                    <Form.Control
                        type="text"
                        value={searchUsername}
                        onChange={(e) => setSearchUsername(e.target.value)}
                        className="w-100"
                    />
                </Form.Group>
                <Button
                    onClick={handleSearch}
                    className="w-100 custom-button"
                >
                    Search
                </Button>
                {message && <Alert variant="danger">{message}</Alert>}
            </Form>
        </div>
    );
}