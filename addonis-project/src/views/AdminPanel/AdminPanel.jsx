import { useState } from 'react';
import UserSearchForm from './UserSearchForm';
import { updateUserBlockStatus } from '../../services/users.services';
import { Container, Card, Button } from 'react-bootstrap';
import './AdminPanel.css'; 

export default function AdminPanel() {
  const [searchResults, setSearchResults] = useState([]);
  const [blockStatusChanged, setBlockStatusChanged] = useState(false);

  const handleUserFound = (user) => {
    setSearchResults([user]);
    setBlockStatusChanged(false);
  };

  const toggleUserBlockStatus = async (userId, currentBlockStatus) => {
    try {
      const newBlockStatus = !currentBlockStatus;
      await updateUserBlockStatus(userId, newBlockStatus);

      setSearchResults((prevResults) =>
        prevResults.map((user) =>
          user.uid === userId ? { ...user, blocked: newBlockStatus } : user
        )
      );

      setBlockStatusChanged(true);
    } catch (error) {
      console.error('Error toggling user block status:', error);
    }
  };

  return (
    <Container className="adminPanel">
      <p className="mx-auto fs-3 my-5 fw-medium custom-title">Admin Panel</p>
      <UserSearchForm onUserFound={handleUserFound} />
      {searchResults.length > 0 && (
        <Card className="custom-card">
          <Card.Body>
            <Card.Title>Search Result</Card.Title>
            <Card.Text>
              <p>User Username: {searchResults[0].username}</p>
              <p>Creation Date: {new Date(searchResults[0].createdOn).toLocaleDateString()}</p>
              <p>Blocked: {searchResults[0].blocked ? 'Yes' : 'No'}</p>
              {blockStatusChanged && (
                <p>User block status changed successfully.</p>
              )}
              <Button
                className="custom-button"
                onClick={() => toggleUserBlockStatus(searchResults[0].uid, searchResults[0].blocked)}
              >
                Toggle Block Status
              </Button>
            </Card.Text>
          </Card.Body>
        </Card>
      )}
    </Container>
  );
}