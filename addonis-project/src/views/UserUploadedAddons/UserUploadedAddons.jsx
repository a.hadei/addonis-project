import  { useEffect, useState, useContext } from "react";
import { AuthContext } from "../../context/AuthContext";
import { ref, onValue, update, get } from "firebase/database";
import { db } from "../../firebase-config/firebase-config";
import EditAddonModal from "./EditAddonModal";
import "./UserUploadedAddons.css";

const UserUploadedAddons = () => {
  const { user } = useContext(AuthContext);
  const [userAddons, setUserAddons] = useState([]);
  const [editingAddon, setEditingAddon] = useState(null);
  const [showEditModal, setShowEditModal] = useState(false);
  const [userData, setUserData] = useState(null);
  const [deleteSuccess, setDeleteSuccess] = useState(false);
  const [editSuccess, setEditSuccess] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const addonsPerPage = 9;
  const [isLoading, setIsLoading] = useState(true);
  const [initialDataLoaded, setInitialDataLoaded] = useState(false);

  useEffect(() => {
    if (!user) {
      return;
    }

    const userRef = ref(db, `users/${user.uid}`);

    get(userRef)
      .then((userSnapshot) => {
        const userData = userSnapshot.val();
        console.log("User Data:", userData);

        if (userData && userData.role) {
          console.log("User Role:", userData.role);
          setUserData(userData);
        }

        setIsLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching user data:", error);
        setIsLoading(false);
      });
  }, [user]);

  useEffect(() => {
    if (!userData) {
      return;
    }

    const userAddonsRef = ref(db, "addons");
    onValue(
      userAddonsRef,
      (snapshot) => {
        const addonData = snapshot.val();
        const userUploadedAddons = Object.entries(addonData).map(([key, value]) => ({
          key,
          ...value,
        }));

        let filteredAddons = [];

        console.log("User Role in useEffect:", userData?.role);

        if (userData?.role === "admin") {
          filteredAddons = userUploadedAddons;
        } else {
          filteredAddons = userUploadedAddons.filter(
            (addon) => addon.userUID === user.uid && !addon.isDeleted
          );
        }

        setUserAddons(filteredAddons);
        setInitialDataLoaded(true);
      },
      (error) => {
        console.error("Error fetching addons from Firebase:", error);
      }
    );
  }, [userData]);

  const handleEditAddon = (addon) => {
    setEditingAddon(addon);
    setShowEditModal(true);
  };

  const handleCloseEditModal = () => {
    setShowEditModal(false);
    setEditingAddon(null);
  };

  const onDeleteAddon = (addon) => {
    const matchingAddon = userAddons.find((userAddon) => userAddon.key === addon.key);

    if (matchingAddon) {
      const addonRef = ref(db, `addons/${matchingAddon.key}`);

      update(addonRef, { isDeleted: true })
        .then(() => {
          console.log("Addon was deleted!");
          setDeleteSuccess(true);
          setTimeout(() => {
            setDeleteSuccess(false);
          }, 5000);
        })
        .catch((error) => {
          console.error("Error marking addon as deleted:", error);
        });
    }
  };

  const onSave = (editedAddon) => {
    console.log("Editing Addon:", editedAddon);

    const updatedFields = {
      name: editedAddon.name,
      description: editedAddon.description,
      creator: editedAddon.creator,
      tags: editedAddon.tags,
      iconLink: editedAddon.iconLink,
    };

    console.log("Updated Fields:", updatedFields);

    const matchingAddon = userAddons.find(
      (addon) => addon.key === editedAddon.key
    );

    if (matchingAddon) {
      const addonRef = ref(db, `addons/${matchingAddon.key}`);

      update(addonRef, updatedFields)
        .then(() => {
          console.log("Addon updated successfully in Firebase!");
          setEditSuccess(true);
          setTimeout(() => {
            setEditSuccess(false);
          }, 5000);
          handleCloseEditModal();
        })
        .catch((error) => {
          console.error("Error updating addon in Firebase:", error);
        });
    }
  };

  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  const currentAddons = userAddons
    .filter((addon) => !addon.isDeleted)
    .slice(indexOfFirstAddon, indexOfLastAddon);

  return (
    <>
      {isLoading ? (
        <p>Loading user data...</p>
      ) : userData ? (
        <div className="container mt-4 my-addons-page">
          <p className="mx-auto fs-3 my-5 fw-medium custom-title">My Addons</p>
          {deleteSuccess && (
            <div className="alert alert-success">Addon deleted successfully!</div>
          )}
          {editSuccess && (
            <div className="alert alert-success">Addon edited successfully!</div>
          )}
          <div className="row">
            {currentAddons.map((addon, index) => (
              <div key={index} className="col-md-4 mb-3">
                <div className="card h-100">
                  <img
                    src={addon.iconLink}
                    alt={`${addon.name} Icon`}
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h5 className="card-title">Addon Name: {addon.name}</h5>
                    <p className="card-text">Creator: {addon.creator}</p>
                    <p className="card-text">Description: {addon.description}</p>
                    <p className="card-text">Tags: {addon.tags}</p>
                  </div>
                  <div className="card-footer d-flex justify-content-between custom-card-footer">
                    <div>
                      {userData && !userData.blocked && (
                        <button
                          className="btn btn-primary custom-button"
                          onClick={() => handleEditAddon(addon)}
                        >
                          Edit
                        </button>
                      )}
                    </div>
                    <div>
                      {userData && !userData.blocked && (
                        <button
                          className="btn custom-button custom-delete-button"
                          onClick={() => onDeleteAddon(addon)}
                        >
                          Delete
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            ))}
            {editingAddon && (
              <EditAddonModal
                show={showEditModal}
                onHide={handleCloseEditModal}
                addon={editingAddon}
                onSave={onSave}
              />
            )}
          </div>
          <nav>
            <ul className="pagination justify-content-center mt-3">
              {Array(Math.ceil(userAddons.length / addonsPerPage))
                .fill()
                .map((_, index) => (
                  <li
                    key={index}
                    className={`page-item ${
                      currentPage === index + 1 ? "active" : ""
                    }`}
                  >
                    <button
                      className="page-link custom-button"
                      onClick={() => setCurrentPage(index + 1)}
                    >
                      {index + 1}
                    </button>
                  </li>
                ))}
            </ul>
          </nav>
        </div>
      ) : (
        <p>User data not available.</p>
      )}
    </>
  );
};

export default UserUploadedAddons;
