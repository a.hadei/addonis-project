import { useState } from "react";
import { Modal, Button, Form, Alert } from "react-bootstrap";
import { uploadBytes, getDownloadURL, ref } from "firebase/storage";
import { storage, db } from "../../firebase-config/firebase-config";
import { update, ref as dbRef } from "firebase/database";
import "./EditAddonModal.css";

const githubToken = "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"

const EditAddonModal = ({ show, onHide, addon, onSave }) => {
  const [newImage, setNewImage] = useState(null);
  const [newAddonVersion, setNewAddonVersion] = useState(null);
  const [editedAddon, setEditedAddon] = useState(addon);
  const [message, setMessage] = useState("");
  const [messageType, setMessageType] = useState("");

  const clearMessage = () => {
    setMessage("");
    setMessageType("");
  };

  const handleSave = async () => {
    if (newImage) {
      const storageRef = ref(storage, `addons/${addon.key}/new-image.jpg`);
      try {
        await uploadBytes(storageRef, newImage);
        const imageURL = await getDownloadURL(storageRef);
        editedAddon.iconLink = imageURL;
      } catch (error) {
        console.error("Error uploading new image:", error);
        setMessage("Error uploading new image: " + error.message);
        setMessageType("error");
        return;
      }
    }

    onSave(editedAddon);
  };

  const handleUploadNewVersion = async () => {
    if (newAddonVersion) {
      const githubUrl = `https://api.github.com/repos/TeodosiIvanov/Addon-App-Store/contents/Addons/${newAddonVersion.name}`;

      try {
        const fileContent = await newAddonVersion.arrayBuffer();
        const encodedContent = btoa(
          new Uint8Array(fileContent).reduce(
            (data, byte) => data + String.fromCharCode(byte),
            ""
          )
        );

        const response = await fetch(githubUrl, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/vnd.github+json",
            Authorization: `Bearer ${githubToken}`,
          },
          body: JSON.stringify({
            message: "Upload new addon version",
            content: encodedContent,
          }),
        });

        if (!response.ok) {
          console.error("Error uploading addon version to GitHub:", response);
          setMessage("Error uploading addon version to GitHub.");
          setMessageType("error");
          return;
        }
        const githubResponse = await response.json();
        const newDownloadUrl = githubResponse.content.download_url;

        const addonRef = dbRef(db, `addons/${addon.key}`);
        update(addonRef, { downloadUrl: newDownloadUrl });

        setMessage("New version uploaded to GitHub and download URL updated.");
        setMessageType("success");
      } catch (error) {
        console.error("Error uploading addon version:", error);
        setMessage("Failed to upload the new version. Please try again later.");
        setMessageType("error");
      }
    }
  };

  return (
    <Modal show={show} onHide={onHide} centered>
      <Modal.Header closeButton className="custom-modal-header">
        <Modal.Title className="custom-title">Edit Addon</Modal.Title>
      </Modal.Header>
      <Modal.Body className="custom-content custom-modal-body">
        {message && (
          <Alert variant={messageType} onClose={clearMessage} dismissible>
            {message}
          </Alert>
        )}
        <Form>
          <Form.Group controlId="newImage">
            <Form.Label>New Addon Image:</Form.Label>
            <Form.Control
              type="file"
              accept="image/*"
              onChange={(e) => setNewImage(e.target.files[0])}
            />
          </Form.Group>
          <Form.Group controlId="addonName">
            <Form.Label>Addon Name:</Form.Label>
            <Form.Control
              type="text"
              value={editedAddon.name}
              onChange={(e) =>
                setEditedAddon({ ...editedAddon, name: e.target.value })
              }
            />
          </Form.Group>
          <Form.Group controlId="addonDescription">
            <Form.Label>Addon Description:</Form.Label>
            <Form.Control
              as="textarea"
              value={editedAddon.description}
              onChange={(e) =>
                setEditedAddon({ ...editedAddon, description: e.target.value })
              }
            />
          </Form.Group>
          <Form.Group controlId="addonCreator">
            <Form.Label>Addon Creator:</Form.Label>
            <Form.Control
              type="text"
              value={editedAddon.creator}
              onChange={(e) =>
                setEditedAddon({ ...editedAddon, creator: e.target.value })
              }
            />
          </Form.Group>
          <Form.Group controlId="addonTags">
            <Form.Label>Addon Tags (comma-separated):</Form.Label>
            <Form.Control
              type="text"
              value={editedAddon.tags}
              onChange={(e) =>
                setEditedAddon({ ...editedAddon, tags: e.target.value })
              }
            />
          </Form.Group>
          <Form.Group controlId="newAddonVersion">
            <Form.Label>New Addon Version:</Form.Label>
            <Form.Control
              type="file"
              accept=".zip"
              onChange={(e) => setNewAddonVersion(e.target.files[0])}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer className="custom-modal-footer">
        <div className="buttons">
          <Button className="custom-button btn-success" onClick={handleSave}>
            Save Changes
          </Button>
          <Button className="custom-button" onClick={handleUploadNewVersion}>
            Upload New Version
          </Button>
        </div>
        <Button className="custom-button" onClick={onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditAddonModal;