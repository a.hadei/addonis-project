import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Carousel } from "react-bootstrap";
import { db } from '../../firebase-config/firebase-config';
import { ref, onValue } from 'firebase/database';
import AddonDetails from "../../components/AddonDetails/AddonDetails";
import "./Home.css"


const Home = () => {
	const [filterText, setFilterText] = useState("");
	const [sortBy, setSortBy] = useState("date");
	const [currentPage, setCurrentPage] = useState(1);
	const addonsPerPage = 8;

	const [addons, setAddons] = useState([]);
	const [recentAddons, setRecentAddons] = useState([]);
	const [downloadedAddons, setDownloadedAddons] = useState([]);
	const [recommendedAddons, setRecommendedAddons] = useState([]);

	const addonsRef = ref(db, 'addons');

	useEffect(() => {
		const unsubscribe = onValue(addonsRef, (snapshot) => {
			if (snapshot.exists()) {
				const addonData = snapshot.val();
				const addonList = Object.entries(addonData).map(([key, value]) => ({
					key,
					...value,
				}));

				const approvedAddons = addonList.filter((addon) => addon.status === "approved" && addon.downloadUrl && !addon.isDeleted);
				setAddons(approvedAddons);

				const recent = approvedAddons.slice(-10);
				const downloaded = approvedAddons.slice().sort((a, b) => b.downloads - a.downloads).slice(0, 10);
				const recommended = approvedAddons.filter((addon) => addon.isRecomended === true).slice(0, 10);

				setRecentAddons(recent);
				setDownloadedAddons(downloaded);
				setRecommendedAddons(recommended);
			} else {
				setAddons([]);
				setRecentAddons([]);
				setDownloadedAddons([]);
				setRecommendedAddons([]);
			}
		});

		return () => unsubscribe();
	}, []);

	const filteredAddons = addons
		.filter((addon) =>
			addon.name.toLowerCase().includes(filterText.trim()) ||
			addon.creator.toLowerCase().includes(filterText.trim()) ||
			(Array.isArray(addon.tags) && addon.tags.some(tag => tag.toLowerCase().includes(filterText.trim())))
		)
		.filter((addon) => !addon.isDeleted);

	filteredAddons.sort((a, b) => {

		if (sortBy === "name") {

			return a.name.localeCompare(b.name);

		}

		if (sortBy === "creator") {

			return a.creator.localeCompare(b.creator);

		}

		if (sortBy === "tags") {

			if (Array.isArray(a.tags) && Array.isArray(b.tags)) {

				const tagsA = a.tags.join(", ");

				const tagsB = b.tags.join(", ");

				return tagsA.localeCompare(tagsB);

			}

			return 0;

		}

		if (sortBy === "date") {

			return b.timestamp - a.timestamp;

		}

		return b.downloads - a.downloads;

	});

	const indexOfLastAddon = currentPage * addonsPerPage;
	const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
	const currentAddons = filteredAddons.slice(indexOfFirstAddon, indexOfLastAddon);

	const buildContent = () => {
		if (currentAddons.length) {
			return (
				<Row>
					{currentAddons.map((addon, index) => (
						<Col md={4} lg={3} key={index} className="mb-4">
							<AddonDetails addon={addon} />
						</Col>
					))}
				</Row>)
		}

		return (<p className="mt-5" style={{ minHeight: "20rem" }}>No results found.</p>)
	}

	return (
		<div className="container mx-auto">
			<div className="d-flex justify-content-center mb-4">
				<div>
					<h5 className="custom-title">Recent</h5>
					<Carousel style={{ maxWidth: "350px", height: "600px" }}>
						{recentAddons.map((addon, index) => (
							<Carousel.Item key={index}>
								<AddonDetails addon={addon} />
							</Carousel.Item>
						))}
					</Carousel>
				</div>

				<div>
					<h5 className="custom-title">Popular</h5>
					<Carousel style={{ maxWidth: "350px", height: "600px" }}>
						{downloadedAddons.map((addon, index) => (
							<Carousel.Item key={index}>
								<AddonDetails addon={addon} />
							</Carousel.Item>
						))}
					</Carousel>
				</div>

				<div>
					<h5 className="custom-title">Recommended</h5>
					<Carousel style={{ maxWidth: "350px", height: "600px" }}>
						{recommendedAddons.map((addon, index) => (
							<Carousel.Item key={index}>
								<AddonDetails addon={addon} />
							</Carousel.Item>
						))}
					</Carousel>
				</div>
			</div>
			<h2 className="mx-auto fs-3 my-5 fw-medium custom-title">All Addons</h2>
			<div className="d-flex mb-3 flex-row">
				<input
					type="text"
					className="form-control me-3"
					placeholder="Search..."
					value={filterText}
					onChange={(e) => {
						setCurrentPage(1)
						setFilterText(e.target.value.toLowerCase())
					}}
				/>
				<span className="my-auto fw-bold custom-title" style={{ minWidth: "5rem" }}>Sort By:</span>
				<select
					className="form-select"
					style={{ width: "10rem" }}
					value={sortBy}
					onChange={(e) => setSortBy(e.target.value)}
				>
					<option value="date">Date</option>
					<option value="name">Name</option>
					<option value="creator">Creator</option>
					<option value="tags">Tags</option>
					<option value="download">Downloads</option>
				</select>
			</div>
			{buildContent()}
			<nav>
				<ul className="pagination justify-content-center mt-3">
					{Array(Math.ceil(filteredAddons.length / addonsPerPage))
						.fill()
						.map((_, index) => (
							<li key={index} className={`page-item ${currentPage === index + 1 ? "active" : ""}`}>
								<button className="page-link custom-button" onClick={() => setCurrentPage(index + 1)}>
									{index + 1}
								</button>
							</li>
						))}
				</ul>
			</nav>
		</div>
	);
}
export default Home;