import { useState, useEffect } from 'react';
import { ref, get, orderByChild, query, equalTo, update } from 'firebase/database';
import { db } from '../../firebase-config/firebase-config';
import "./AdminDashboard.css"

const AdminDashboard = () => {
  const [addons, setAddons] = useState([]);
  const [sortByStatus, setSortByStatus] = useState('pending');
  const [selectedAddonKey, setSelectedAddonKey] = useState('');
  const [statusChangeMessage, setStatusChangeMessage] = useState('');
  const [isStatusModified, setIsStatusModified] = useState(false);
  const [isRecomendedModified, setIsRecomendedModified] = useState(false);

  useEffect(() => {
    const addonsRef = ref(db, 'addons');
    const addonsQuery = query(
      addonsRef,
      orderByChild('status'),
      equalTo(sortByStatus)
    );

    get(addonsQuery).then((snapshot) => {
      if (snapshot.exists()) {
        const addonData = snapshot.val();
        const addonList = Object.entries(addonData).map(([key, value]) => ({
          key,
          ...value,
          newStatus: value.status,
          newIsRecomended: value.isRecommended,
        }));
        setAddons(addonList);
      } else {
        setAddons([]);
      }
    });
  }, [sortByStatus, statusChangeMessage]);

  const handleSaveStatusChanges = () => {
    if (selectedAddonKey && (isStatusModified || isRecomendedModified)) {
      const addonRef = ref(db, `addons/${selectedAddonKey}`);
      const selectedAddon = addons.find((addon) => addon.key === selectedAddonKey);

      const updates = {
        status: selectedAddon.newStatus,
        isRecomended: selectedAddon.newIsRecomended,
      };

      update(addonRef, updates)
        .then(() => {
          setSelectedAddonKey('');
          setStatusChangeMessage('Status and/or Recommended status changed successfully');
          setSortByStatus(selectedAddon.newStatus);
        })
        .catch((error) => {
          console.error("Error changing status and/or Recommended status:", error);
          setStatusChangeMessage('Error changing status and/or Recommended status');
        });
    }
  };

  return (
    <div className="bord">
    <div className="container">
    <p className="mx-auto fs-3 my-5 fw-medium custom-title">Admin Dashboard</p>
      {statusChangeMessage && <p>{statusChangeMessage}</p>}
      <div className="mb-3">
        <label htmlFor="sortByStatus" className="form-label">
          Sort by Status:
        </label>
        <select
          id="sortByStatus"
          className="form-select"
          value={sortByStatus}
          onChange={(e) => setSortByStatus(e.target.value)}
        >
          <option value="pending">Pending</option>
          <option value="approved">Approved</option>
          <option value="rejected">Rejected</option>
          <option value="in-progress">In Progress</option>
        </select>
      </div>

      <table className="table table-striped custom-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Status</th>
            <th>Is Recommended</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {addons.map((addon) => (
            <tr key={addon.key}>
              <td>{addon.name}</td>
              <td>{addon.description}</td>
              <td>
                <select
                  className="form-select"
                  value={addon.newStatus}
                  onChange={(e) => {
                    const newStatus = e.target.value;
                    const updatedAddons = addons.map((addonItem) =>
                      addonItem.key === addon.key
                        ? { ...addonItem, newStatus }
                        : addonItem
                    );
                    setAddons(updatedAddons);
                    setIsStatusModified(true);
                  }}
                >
                  <option value="pending">Pending</option>
                  <option value="approved">Approved</option>
                  <option value="rejected">Rejected</option>
                  <option value="in-progress">In Progress</option>
                </select>
              </td>
              <td>
                <select
                  className="form-select"
                  value={addon.newIsRecomended}
                  onChange={(e) => {
                    const newIsRecomended = e.target.value === 'true';
                    const updatedAddons = addons.map((addonItem) =>
                      addonItem.key === addon.key
                        ? { ...addonItem, newIsRecomended }
                        : addonItem
                    );
                    setAddons(updatedAddons);
                    setIsRecomendedModified(true);
                  }}
                >
                  <option value="false">Not Recommended</option>
                  <option value="true">Recommended</option>
                </select>
              </td>
              <td>
                <button
                  className="btn custom-button"
                  onClick={() => setSelectedAddonKey(addon.key)}
                >
                  Save Changes
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <button className="btn custom-button" onClick={handleSaveStatusChanges}>
        Confirm
      </button>
    </div>
    </div>
  );
};

export default AdminDashboard;