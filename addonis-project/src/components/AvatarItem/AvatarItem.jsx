import Stack from "react-bootstrap/Stack";
import './AvatarItem.css';

AvatarItem.propTypes = {
  imageUrl: "",
  name: "",
  email: "",
};

export default function AvatarItem({ imageUrl, name, email }) {
  return (
    <div>
      <Stack direction="horizontal" gap={3}>
        <div >
          <img className="avatar" src={imageUrl} />
        </div>

        <div className="d-flex flex-column align-items-start">
          <div className="fw-bold">{name}</div>
          <div>{email}</div>
        </div>
      </Stack>
    </div>
  );
}
