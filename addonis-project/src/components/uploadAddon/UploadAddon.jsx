import { useState, useContext, useEffect } from "react";
import { AuthContext } from "../../context/AuthContext";
import { ref, serverTimestamp, push, onValue, get } from "firebase/database";
import { db, storage } from "../../firebase-config/firebase-config";
import { Form, Button } from "react-bootstrap";
import { ref as storageRef, uploadBytes, getDownloadURL } from "firebase/storage";
import Loader from "./Loader";
import "./UploadAddon.css"


const GITHUB_TOKEN = "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";

const UploadAddon = () => {
  const [addonName, setAddonName] = useState("");
  const [addonDescription, setAddonDescription] = useState("");
  const [addonCreator, setAddonCreator] = useState("");
  const [addonTags, setAddonTags] = useState("");
  const [addonImage, setAddonImage] = useState(null);
  const [zipFile, setZipFile] = useState(null);
  const [isUploading, setIsUploading] = useState(false);
  const { user } = useContext(AuthContext);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [downloadUrl, setDownloadUrl] = useState("");
  const [originLink, setOriginLink] = useState("");
  const [openIssuesCount, setOpenIssuesCount] = useState(null);
  const [pullRequestsCount, setPullRequestsCount] = useState(null);
  const [lastCommit, setLastCommit] = useState(null);

  const showSuccessMessage = (message) => {
    setSuccessMessage(message);
    setTimeout(() => {
      setSuccessMessage("");
    }, 5000);
  };

  const showErrorMessage = (message) => {
    setErrorMessage(message);
    setTimeout(() => {
      setErrorMessage("");
    }, 5000);
  };

  useEffect(() => {
    const addonsRef = ref(db, "addons");
    onValue(
      addonsRef,
      (snapshot) => {
        const addonData = snapshot.val();
        console.log("Addon Data in Firebase:", addonData);
      },
      (error) => {
        console.error("Error fetching addon data from Firebase:", error);
      }
    );
  }, []);

  const getSHAHashes = async () => {
    const url =
      "https://api.github.com/repos/TeodosiIvanov/Addon-App-Store/contents/Addons";

    try {
      const response = await fetch(url, {
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${GITHUB_TOKEN}`,
          "X-GitHub-Api-Version": "2022-11-28",
          "Content-Type": "application/xml",
        },
      });

      if (!response.ok) {
        throw new Error("Error fetching SHA hashes from GitHub.");
      }

      const data = await response.json();

      const hashes = data.map((file) => ({
        name: file.name,
        sha: file.sha,
      }));

      return hashes;
    } catch (error) {
      console.error("Error fetching SHA hashes:", error.message);
      return [];
    }
  };

  const handleZipFileChange = (e) => {
    if (e.target.files[0]) {
      setZipFile(e.target.files[0]);
    }
  };

  const handleAddonImageChange = (e) => {
    if (e.target.files[0]) {
      setAddonImage(e.target.files[0]);
    }
  };

  const uploadAddonImage = async () => {
    if (!addonImage) {
      return;
    }

    try {
      const imageName = `${addonName}_${Date.now()}_${addonImage.name}`;
      const imageRef = storageRef(storage, `addons/${addonName}/${imageName}`);

      await uploadBytes(imageRef, addonImage);

      const imageURL = await getDownloadURL(imageRef);
      return imageURL;
    } catch (error) {
      console.error("Error uploading addon image:", error);
      return null;
    }
  };

  const fetchGitHubData = async () => {
    if (!originLink) {
      showErrorMessage("Please provide the Origin link to a GitHub repository.");
      return;
    }

    try {
      const parts = originLink.split("/");
      const username = parts[3];
      const repoName = parts[4];

      const repoUrl = `https://api.github.com/repos/${username}/${repoName}`;
      const response = await fetch(repoUrl, {
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${GITHUB_TOKEN}`,
          "X-GitHub-Api-Version": "2022-11-28",
          "Content-Type": "application/xml",
        },
      });

      if (!response.ok) {
        throw new Error("Error fetching repository data from GitHub.");
      }

      const data = await response.json();
      console.log("GitHub Repository Data:", data);

      const pullRequestsUrl = `https://api.github.com/repos/${username}/${repoName}/pulls`;
      const pullRequestsResponse = await fetch(pullRequestsUrl, {
        headers: {
          Authorization: `Bearer ${GITHUB_TOKEN}`,
        },
      });

      if (!pullRequestsResponse.ok) {
        throw new Error("Error fetching pull requests data from GitHub.");
      }

      const pullRequestsData = await pullRequestsResponse.json();
      console.log("Pull Requests Data:", pullRequestsData);

      const openIssuesCount = data.open_issues_count;

      console.log("Open Issues Count:", openIssuesCount);

      const commitsUrl = `https://api.github.com/repos/${username}/${repoName}/commits`;
      const commitsResponse = await fetch(commitsUrl, {
        headers: {
          Authorization: `Bearer ${GITHUB_TOKEN}`,
        },
      });

      if (!commitsResponse.ok) {
        throw new Error("Error fetching commits data from GitHub.");
      }

      const commitsData = await commitsResponse.json();
      const lastCommitDate = commitsData[0].commit.author.date;
      const formattedLastCommitDate = formatCommitDate(lastCommitDate);
      console.log("Last Commit Date:", formattedLastCommitDate);

      setPullRequestsCount(pullRequestsData.length);
      setLastCommit(formattedLastCommitDate);
      setOpenIssuesCount(openIssuesCount);

      showSuccessMessage("Fetched data from the GitHub repository successfully!");
    } catch (error) {
      console.error("Error fetching data from GitHub repository:", error.message);
      showErrorMessage("Error fetching data from GitHub repository.");
    }
  };

  const uploadAddonToGitHub = async () => {
    if (!zipFile || isUploading) {
      return;
    }

    setIsUploading(true);

    try {
      const fileContent = await zipFile.arrayBuffer();
      const encodedContent = btoa(
        new Uint8Array(fileContent).reduce(
          (data, byte) => data + String.fromCharCode(byte),
          ""
        )
      );
      const githubUrl = `https://api.github.com/repos/TeodosiIvanov/Addon-App-Store/contents/Addons/${zipFile.name}`;

      const currentSHAHashes = await getSHAHashes();

      const matchingSHAHash = currentSHAHashes.find(
        (file) => file.name === zipFile.name
      );

      const rawGithubResponse = await fetch(githubUrl, {
        method: matchingSHAHash ? "PATCH" : "PUT",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${GITHUB_TOKEN}`,
        },
        body: JSON.stringify({
          message: "Upload addon zip file",
          content: encodedContent,
          sha: matchingSHAHash ? matchingSHAHash.sha : null,
        }),
      });

      if (!rawGithubResponse.ok) {
        const githubErrorData = await rawGithubResponse.json();
        console.error("Error uploading addon to GitHub:", githubErrorData.message);
        showErrorMessage("Error uploading addon to GitHub");
        return;
      }

      const githubResponse = await rawGithubResponse.json();
      setDownloadUrl(githubResponse.content.download_url);

      console.log("Addon uploaded to GitHub successfully!");
      showSuccessMessage("Addon uploaded to GitHub successfully!");
      return githubResponse.content.download_url;
    } catch (error) {
      console.error("Error uploading addon to GitHub:", error.message);
      showErrorMessage("Error uploading addon to GitHub");
    } finally {
      setIsUploading(false);
    }
  };

  const uploadAddon = async () => {
    if (user && user.uid) {
      const userRef = ref(db, `users/${user.uid}`);
      const userSnapshot = await get(userRef);

      if (userSnapshot.exists()) {
        const userData = userSnapshot.val();

        if (userData.blocked) {
          showErrorMessage("You are blocked and cannot upload addons.");
          return;
        }
      }
    }

    if (
      !addonName ||
      !addonDescription ||
      !addonCreator ||
      !addonImage ||
      !zipFile ||
      !originLink
    ) {
      showErrorMessage("Please provide all addon details including the Origin link.");
      return;
    }

    if (!user) {
      showErrorMessage("Please log in to upload an addon.");
      return;
    }

    try {
      const addonImageURL = await uploadAddonImage();
      if (!addonImageURL) {
        console.error("Failed to upload addon image.");
        showErrorMessage("Failed to upload the addon image. Please try again later.");
        return;
      }

      const githubDownloadUrl = await uploadAddonToGitHub();

      const addonsRef = ref(db, "addons");

      const addonData = {
        name: addonName,
        description: addonDescription,
        rating: 0,
        downloads: 0,
        creator: addonCreator,
        downloadUrl: githubDownloadUrl,
        tags: addonTags.split(",").map((tag) => tag.trim()),
        iconLink: addonImageURL,
        timestamp: serverTimestamp(),
        userUID: user.uid,
        status: "pending",
        isDeleted: false,
        isRecommended: false,
        originLink: originLink,
        openIssues: openIssuesCount,
        pullRequests: pullRequestsCount,
        lastCommit: lastCommit,
        totalRate: 0,
        totalVoters: 0,
      };

      await push(addonsRef, addonData);
      setAddonName("");
      setAddonDescription("");
      setAddonCreator("");
      setAddonTags("");
      setOriginLink("");

      showSuccessMessage("Addon uploaded to Firebase successfully!");
    } catch (error) {
      console.error("Error uploading addon:", error);
      showErrorMessage("Error uploading addon to Firebase");
    }
  };

  function formatCommitDate(isoDate) {
    const date = new Date(isoDate);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const formattedDate = `${date.getFullYear()} - ${date.toLocaleString('default', { month: 'short' })} - ${date.getDate()}:${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes} ${hours >= 12 ? 'PM' : 'AM'}`;
    return formattedDate;
  }

  return (
    <div className="container mt-4">
      <p className="mx-auto fs-3 my-5 fw-medium custom-title">Upload Addon</p>
      <div className="col-md-6 mx-auto">
        <Form>
          <Form.Group controlId="addonName">
            <Form.Label>Addon Name</Form.Label>
            <Form.Control
              type="text"
              value={addonName}
              onChange={(e) => setAddonName(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="addonDescription">
            <Form.Label>Addon Description</Form.Label>
            <Form.Control
              as="textarea"
              value={addonDescription}
              onChange={(e) => setAddonDescription(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="addonCreator">
            <Form.Label>Addon Creator</Form.Label>
            <Form.Control
              type="text"
              value={addonCreator}
              onChange={(e) => setAddonCreator(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="addonTags">
            <Form.Label>Addon Tags (comma-separated)</Form.Label>
            <Form.Control
              type="text"
              value={addonTags}
              onChange={(e) => setAddonTags(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="addonImage">
            <Form.Label>
              Select an Addon Image (PNG, JPEG, etc.):
            </Form.Label>
            <Form.Control
              type="file"
              accept="image/*"
              onChange={handleAddonImageChange}
            />
          </Form.Group>
          <Form.Group controlId="zipFile">
            <Form.Label>Select a ZIP file:</Form.Label>
            <Form.Control
              type="file"
              accept=".zip"
              onChange={handleZipFileChange}
            />
          </Form.Group>
          <Form.Group controlId="originLink">
            <Form.Label>Origin Link (GitHub Repository)</Form.Label>
            <Form.Control
              type="text"
              value={originLink}
              onChange={(e) => setOriginLink(e.target.value)}
            />
          </Form.Group>


          <div className="d-flex justify-content-center">
            <Button onClick={fetchGitHubData} className="custom-button mx-2">
              Fetch GitHub Info
            </Button>

            <Button onClick={uploadAddon} className="custom-button mx-2">
              Upload Addon
            </Button>
          </div>

          {isUploading && (
            <div className="loader-container">
              <Loader />
            </div>
          )}

          {successMessage && (
            <p className="text-success">{successMessage}</p>
          )}
          {errorMessage && <p className="text-danger">{errorMessage}</p>}
        </Form>
      </div>
    </div>
  );
}

export default UploadAddon;
