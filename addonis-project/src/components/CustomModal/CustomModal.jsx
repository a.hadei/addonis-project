import './CustomModal.css'; 

const CustomModal = ({ message }) => {
  return (
    <div className="custom-modal">
      <div className="modal-content custom-content">
        <p>{message}</p>
      </div>
    </div>
  );
};

export default CustomModal;