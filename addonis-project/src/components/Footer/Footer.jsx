import { Container, Row, Col } from "react-bootstrap";
import { BsFacebook, BsTwitter, BsInstagram } from "react-icons/bs";
import "./Footer.css";


export default function Footer() {
  const today = new Date();
  const iconSize = "23px";
  return (
    <div className="footer py-3">
      <Container>
        <Row className="justify-content-center align-items-center">
          <Col xs="auto" className="mb-3">
            <a className="text-decoration-none custom-content" href="/about">
              About
            </a>
          </Col>
          <Col xs="auto" className="mb-3">
            <a
              className="text-decoration-none custom-content"
              href="/contactus"
            >
              Contact
            </a>
          </Col>
        </Row>
        <p className="custom-content">Follow us</p>
        <Row className="justify-content-center align-items-center">
        <Col xs="auto" className="mb-3">
            <a href="https://www.facebook.com/">
              <BsFacebook className="facebook-icon" alt="Facebook" size={iconSize}/>
            </a>
          </Col>
          <Col xs="auto" className="mb-3">
            <a href="https://www.twitter.com/">
              <BsTwitter className="twitter-icon" alt="Twitter" size={iconSize}/>
            </a>
          </Col>
          <Col xs="auto" className="mb-3">
            <a href="https://www.instagram.com/">
              <BsInstagram className="instagram-icon" alt="Instagram" size={iconSize}/>
            </a>
          </Col>
        </Row>
      </Container>
      <p className="text-center m-0 custom-content">
        © {today.getFullYear()} Team 10. All rights reserved.
      </p>
    </div>
  );
}
