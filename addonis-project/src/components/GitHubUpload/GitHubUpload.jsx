import { useState } from "react";


const GitHubUpload = ({ onUploadSuccess, onUploadError }) => {
    const [zipFile, setZipFile] = useState(null);
    const [isUploading, setIsUploading] = useState(false);
    const [shaHashes, setSHAHashes] = useState([]); 
  
    async function getSHAHashes() {
      const url = "https://api.github.com/repos/TeodosiIvanov/Addon-App-Store/contents/Addons";
      
      try {
        const response = await fetch(url, {
          headers: {
            Accept: "application/vnd.github+json",
            Authorization: "Bearer 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",
            "X-GitHub-Api-Version": "2022-11-28",
            "Content-Type": "application/xml"
          }
        });
        
        if (!response.ok) {
          throw new Error("Error fetching SHA hashes from GitHub.");
        }
    
        const data = await response.json();
        
        const hashes = data.map(file => ({
          name: file.name,
          sha: file.sha
        }));
    
        return hashes;
      } catch (error) {
        console.error("Error fetching SHA hashes:", error.message);
        return [];
      }
    }
  
    const handleZipFileChange = (e) => {
      if (e.target.files[0]) {
        setZipFile(e.target.files[0]);
      }
    };
  
    const uploadAddonToGitHub = async () => {
      if (!zipFile || isUploading) {
        return;
      }
  
      setIsUploading(true);
  
      try {
        const fileContent = await zipFile.arrayBuffer();
        const encodedContent = btoa(
          new Uint8Array(fileContent).reduce(
            (data, byte) => data + String.fromCharCode(byte),
            ""
          )
        );
        const githubUrl = `https://api.github.com/repos/TeodosiIvanov/Addon-App-Store/contents/Addons/${zipFile.name}`;
  
        const currentSHAHashes = await getSHAHashes();
  
        const matchingSHAHash = currentSHAHashes.find(
          file => file.name === zipFile.name
        );
  
        const response = await fetch(githubUrl, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/vnd.github+json",
            Authorization: "Bearer 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",
          },
          body: JSON.stringify({
            message: "Upload addon zip file",
            content: encodedContent,
            sha: matchingSHAHash ? matchingSHAHash.sha : null
          }),
        });
  
        if (response.ok) {
          onUploadSuccess("Addon zip file uploaded to GitHub successfully!");
        } else {
          onUploadError(
            "Error uploading addon zip file to GitHub. Please try again."
          );
        }
      } catch (error) {
        onUploadError(
          "Error uploading addon zip file to GitHub: " + error.message
        );
      } finally {
        setIsUploading(false);
      }
    };
  
    return (
      <div>
        <h2>Upload Addon to GitHub</h2>
        <form>
          <label htmlFor="zipFile">Select a ZIP file:</label>
          <input type="file" accept=".zip" onChange={handleZipFileChange} />
          <button
            type="button"
            onClick={uploadAddonToGitHub}
            disabled={isUploading}
          >
            {isUploading ? "Uploading..." : "Upload"}
          </button>
        </form>
      </div>
    );
  };
  
  
  export default GitHubUpload;