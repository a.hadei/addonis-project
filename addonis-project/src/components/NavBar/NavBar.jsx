import 'bootstrap/dist/css/bootstrap.min.css';
import { logoutUser } from '../../services/auth.services'
import { useContext, useEffect, useState } from 'react'
import { Button, Container, Navbar, Nav } from "react-bootstrap";
import { AuthContext } from '../../context/AuthContext'
import { getUserDataByUID } from '../../services/users.services';
import TeamLogo from "../../icons/TeamLogo3.png";
import { Link, useNavigate } from 'react-router-dom';
import "./NavBar.css";

const NavBar = () => {
  const { user, setUser } = useContext(AuthContext);
  const [userData, setUserData] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    if (user) {
      getUserDataByUID(user.uid)
        .then((snapshot) => {
          if (snapshot.exists()) {
            const userData = snapshot.val(Object.keys(snapshot.val())[0]);
            setUserData(userData);
          }
        })
        .catch((error) => {
          console.error('Error fetching user data:', error);
        });
    }
  }, [user]);

  const handleLogout = () => {
    logoutUser().then(() => {
      navigate('/');
    });
  };

  return (
    <>
      <Navbar className="p-0" expand="lg">
        <Container className="shadow p-3 mb-5 rounded custom-container">
          <Navbar.Brand as={Link} to="/" className="custom-navbar-brand" ><img src={TeamLogo} alt="Team Logo" className="logo-image" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" className="navbar-toggler-icon-custom" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              {user && (
                <>
                  <Nav.Link as={Link} to="/uploadaddon" className="nav-link-custom">Upload Addon</Nav.Link>
                  <Nav.Link as={Link} to="/useruploadedaddons" className="nav-link-custom">My Addons</Nav.Link>
                  {user && userData?.role === 'admin' && (
                    <>
                      <Nav.Link as={Link} to="/adminpanel" className="nav-link-custom">Admin Panel</Nav.Link>
                      <Nav.Link as={Link} to="/admindashboard" className="nav-link-custom">Admin Dashboard</Nav.Link>
                    </>
                  )}
                </>
              )}
            </Nav>
            <Nav>
              {user !== null ? (
                <>
                  {userData && (
                    <>
                      <Link to="/editprofile" className="my-auto nav-link-custom greeting-custom">
                        Hello, {userData.firstName} {userData.lastName}
                      </Link>
                      <span>&nbsp;</span>
                    </>
                  )}
                  <Button className="custom-logout-button" onClick={handleLogout}>
                    Log Out
                  </Button>
                </>
              ) : (
                <>
                  <Nav.Link as={Link} to="/signin" className="nav-link-custom">Sign In</Nav.Link>
                  <Nav.Link as={Link} to="/register" className="nav-link-custom">Register</Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default NavBar;