import { useState, useEffect } from "react";
import { Button, Modal } from "react-bootstrap";
import { ref, update, increment, onValue } from "firebase/database";
import { db, auth } from "../../firebase-config/firebase-config";
import { FaStar, FaStarHalfAlt } from "react-icons/fa";
import { AiOutlineStar } from "react-icons/ai";
import { VscGithub, VscGithubAction } from "react-icons/vsc";
import { SiGithubactions } from "react-icons/si";
import { BsCalendarDateFill } from "react-icons/bs";
import { ImFolderUpload } from "react-icons/im";
import { FaCircleInfo, FaUserAstronaut } from "react-icons/fa6";
import "./AddonDetails.css";

const AddonDetail = ({ addon }) => {
  const [downloadCount, setDownloadCount] = useState(addon.downloads || 0);
  const [userRating, setUserRating] = useState(null);
  const [rating, setRating] = useState(null);
  const [hover, setHover] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const timestamp = addon.timestamp;
  const date = new Date(timestamp);
  const formattedDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;

  useEffect(() => {
    const addonRef = ref(db, `addons/${addon.key}/downloads`);
    const user = auth.currentUser;

    if (user) {
      const userRatingRef = ref(db, `addons/${addon.key}/ratings/${user.uid}`);

      onValue(userRatingRef, (snapshot) => {
        const rating = snapshot.val();
        setUserRating(rating);
      });
    }

    const unsubscribe = onValue(addonRef, (snapshot) => {
      const count = snapshot.val() || 0;
      setDownloadCount(count);
    });

    return () => {
      unsubscribe();
    };
  }, [addon.key]);

  const incrementDownloadCount = () => {
    const updates = {};
    updates[`addons/${addon.key}/downloads`] = increment(1);
    update(ref(db), updates).then(() => {
      setDownloadCount(downloadCount + 1);
    });
  };

  const incrementVoters = () => {
    const updateVoters = {};
    updateVoters[`addons/${addon.key}/voters`] = increment(1);
    update(ref(db), updateVoters).then(() => {
      setVoters(voters + 1);
    });
  };

  const rateAddon = (rating) => {
    const user = auth.currentUser;
    if (user) {
      const updates = {};
      updates[`addons/${addon.key}/ratings/${user.uid}`] = rating;
      updates[`addons/${addon.key}/totalVoters`] = increment(1);
      updates[`addons/${addon.key}/totalRate`] = increment(rating);
      update(ref(db), updates);
      setUserRating(rating);
    }
  };

  const openModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  return (
    <div className="card mb-3 p-1 h-100">
      <h5 className="card-title custom-title">{addon.name}</h5>
      <div className="card-body text-start d-flex flex-column">
        <div className="d-flex justify-content-center mb-3">
          <img
            src={addon.iconLink}
            alt={`${addon.name} Icon`}
            className="custom-image"
          />
        </div>
        <p className="custom-content">
          <strong className="text-color">Downloads:</strong> <span className="tag-value">{addon.downloads}</span>
        </p>
        <p className="custom-content">
          <strong className="text-color">Tags:</strong> <span className="tag-value">{addon.tags}</span>
        </p>
        <p className="custom-content">
          <strong className="text-color">Rating:</strong>{" "}
          <span className="tag-value">
            {addon.totalRate === 0
              ? "0"
              : (addon.totalRate / addon.totalVoters).toFixed(1) +
              " " +
              "(" +
              `${addon.totalVoters}` +
              " " +
              "votes" +
              ")"}
          </span>
        </p>
        <div className="average-star-rating">
          {Array.from({ length: 5 }).map((_, index) => {
            const number = index + 0.5;
            return (
              <span className="custom-content mb-3" key={index}>
                {addon.totalRate / addon.totalVoters >= index + 1 ? (
                  <FaStar className="average-star" />
                ) : addon.totalRate / addon.totalVoters >= number ? (
                  <FaStarHalfAlt className="average-star-icon" />
                ) : (
                  <AiOutlineStar className="average-star-icon" />
                )}
              </span>
            );
          })}
        </div>
        {auth.currentUser && (
          <p className="custom-content">
            <strong className="text-color">Your Rating:</strong>{" "}
            <span className="tag-value">
              {userRating !== null ? userRating + "/5" : "Not rated"}
            </span>
          </p>
        )}
        {auth.currentUser && (
          <div className="custom-content mb-3">
            {Array.from({ length: 5 }).map((_, index) => {
              index += 1;
              return (
                <button
                  disabled={userRating !== null}
                  type="radio"
                  key={index}
                  className={index <= (hover || rating) ? "on" : "off"}
                  onClick={() => {
                    rateAddon(index);
                    incrementVoters;
                  }}
                  onMouseEnter={() => setHover(index)}
                  onMouseLeave={() => setHover(rating)}
                >
                  <FaStar className="star" />
                </button>
              );
            })}
          </div>
        )}
      </div>

      <Button className="w-100 mt-2 custom-button" onClick={openModal}>
        Info
      </Button>
      <Button
        as="a"
        className="w-100 mt-2 mb-1 custom-button"
        href={addon.downloadUrl}
        onClick={incrementDownloadCount}
      >
        Download
      </Button>

      <Modal show={showModal} onHide={closeModal}>
        <Modal.Header closeButton className="custom-modal-header">
          <Modal.Title className="custom-title" style={{ color: 'white' }}>
            Info for {addon.name}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="custom-modal-body">
          {showModal && (
            <div>
              <p className="custom-title">
                <strong>
                  <ImFolderUpload className="upload icon" alt="reactIcons" size={20} /> Uploaded on:
                </strong>{" "}
                {formattedDate}
              </p>
              <p className="custom-title">
                <strong>
                  <FaCircleInfo className="discription-icon" alt="reactIcons" size={20} /> Description:
                </strong>{" "}
                {addon.description}
              </p>
              <p className="custom-title">
                <strong>
                  <FaUserAstronaut className="creator-icon" alt="reactIcons" size={20} /> Creator:
                </strong>{" "}
                {addon.creator}
              </p>
              <p className="custom-title">
                <strong>
                  <VscGithub className="github-icon" alt="reactIcons" size={20} /> Origin Link:
                </strong>{" "}
                <a
                  href={addon.originLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {/* {addon.originLink} */}GitHub Link
                </a>
              </p>
              <p className="custom-title">
                <strong>
                  <VscGithubAction className="github-issuesIcon" alt="reactIcons" size={20} /> Open Issues Count:
                </strong> {" "}
                {addon.openIssues}
              </p>
              <p className="custom-title">
                <strong>
                  <SiGithubactions className="github-pullRequestsIcon" alt="reactIcons" size={20} /> Pull Requests Count:
                </strong>{" "}
                {addon.pullRequests}
              </p>
              <p className="custom-title">
                <strong>
                  <BsCalendarDateFill className="github-lastCommitDate" alt="reactIcons" size={20} /> Last Commit Date:
                </strong>{" "}
                {addon.lastCommit}
              </p>
            </div>
          )}
        </Modal.Body >
        <Modal.Footer className="custom-modal-footer">
          <Button className="custom-button" onClick={closeModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default AddonDetail;
