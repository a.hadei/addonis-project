import {
  get,
  set,
  ref,
  push,
  update,
  serverTimestamp,
} from "firebase/database";
import { db } from "../firebase-config/firebase-config";

export const getUserDataByUID = (uid) => {
  return get(ref(db, `users/${uid}`));
};

export const createUser = (
  username,
  uid,
  email,
  firstName,
  lastName,
  phoneNumber,
  role = "user",
  blocked = false
) => {
  return set(ref(db, `users/${uid}`), {
    username,
    uid,
    email,
    firstName,
    lastName,
    phoneNumber,
    role,
    blocked,
    profilePictureUrl: "",
    createdOn: Date.now(),
  });
};
export const getUsersByUsername = async (username) => {
  try {
    const usersRef = db.ref("users");
    const snapshot = await usersRef
      .orderByChild("username")
      .equalTo(username)
      .once("value");
    const users = [];

    snapshot.forEach((childSnapshot) => {
      const user = childSnapshot.val();
      users.push(user);
    });

    return users;
  } catch (error) {
    throw error;
  }
};

export const updateUserBlockStatus = async (uid, blocked) => {
  try {
    const userRef = ref(db, `users/${uid}`);
    await update(userRef, { blocked });
  } catch (error) {
    throw error;
  }
};
