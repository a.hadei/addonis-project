import {
  get,
  set,
  ref,
  push,
  update,
  serverTimestamp,
} from "firebase/database";
import { db } from "../firebase-config/firebase-config";

export const updateEmailInFirebase = (uid, newEmail) => {
  const emailRef = ref(db, `users/${uid}`);
  const updates = { email: newEmail };

  return update(emailRef, updates)
    .then(() => {
    })
    .catch((error) => {
      console.error("Error updating email in Firebase:", error);
      throw error;
    });
};

export const updateFirstNameInFirebase = (uid, newFirstName) => {
  const firstNameRef = ref(db, `users/${uid}`);
  const updates = { firstName: newFirstName };

  return update(firstNameRef, updates)
    .then(() => {
    })
    .catch((error) => {
      console.error("Error updating first name in Firebase:", error);
      throw error;
    });
};

export const updateLastNameInFirebase = (uid, newLastName) => {
  const lastNameRef = ref(db, `users/${uid}`);
  const updates = { lastName: newLastName };

  return update(lastNameRef, updates)
    .then(() => {
    })
    .catch((error) => {
      console.error("Error updating last name in Firebase:", error);
      throw error;
    });
};
export const updateProfilePictureInFirebase = async (
  uid,
  profilePictureUrl
) => {
  try {
    const userRef = ref(db, `users/${uid}`);
    await update(userRef, {
      profilePictureUrl: profilePictureUrl,
    });
    return true;
  } catch (error) {
    console.error("Error updating profile picture URL:", error);
    return false;
  }
};
