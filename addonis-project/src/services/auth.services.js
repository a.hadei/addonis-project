import { auth } from "../firebase-config/firebase-config";
import { ref, get } from "firebase/database";
import { db } from "../firebase-config/firebase-config";
import {
  createUserWithEmailAndPassword,
  fetchSignInMethodsForEmail,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";

export const checkIfUserExists = async (email) => {
  let signInMethods = await fetchSignInMethodsForEmail(auth, email);
  return signInMethods.length > 0;
};

export const registerUser = (email, password) => {
  return createUserWithEmailAndPassword(auth, email, password);
};

export const loginUser = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password);
};

export const logoutUser = () => {
  return signOut(auth);
};
export const checkUsernameAvailability = async (username) => {
  try {
    const usersRef = ref(db, "users");
    const usersSnapshot = await get(usersRef);

    const users = Object.values(usersSnapshot.val());
    const usernameExists = users.some((user) => user.username === username);

    return !usernameExists;
  } catch (error) {
    console.error("Error checking username availability:", error);
    throw error;
  }
};

export const checkIfPhoneExists = async (phoneNumber) => {
  try {
    const phoneRef = ref(db, "users");
    const snapshot = await get(phoneRef);

    if (snapshot.exists()) {
      const userData = snapshot.val();
      const userPhoneNumbers = Object.values(userData).map(
        (user) => user.phoneNumber
      );

      return userPhoneNumbers.includes(phoneNumber);
    }

    return false;
  } catch (error) {
    console.error("Error checking if phone number exists:", error);
    throw error;
  }
};
