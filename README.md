# Addon Galaxy - Addon Web Store

Welcome to Addon Galaxy, the ultimate destination for discovering, uploading, and managing addons for your favorite applications and platforms.



## Table of Contents

- [Addon Galaxy - Addon Web Store](#addon-galaxy---addon-web-store)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Features](#features)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
    - [Usage](#usage)
      - [Database](#database)
      - [Technologies Used](#technologies-used)

## Introduction

Addon Galaxy is a web-based platform designed to simplify the process of discovering, sharing, and managing addons, extensions, plugins, or any additional components for your software or application. Whether you're a developer looking to showcase your creations or a user searching for that perfect addon, Addon Galaxy has you covered.

## Features

- **Browse**: Discover a wide range of addons for various applications.
- **Upload**: Share your own addons with the community.
- **User Profiles**: Customize your profile, track your uploads, and manage your addons.
- **Search**: Easily find addons using powerful search functionality.
- **Admin Panel**: If you're an admin, manage user uploads, block users, and more.

## Getting Started

### Prerequisites

Before you begin, ensure you have met the following requirements:

- **Node.js**: Make sure you have Node.js installed. If not, you can download it from [nodejs.org](https://nodejs.org/).

### Installation

1. Clone this repository to your local machine:

   ```bash
   git clone https://github.com/yourusername/addon-galaxy.git
Navigate to the project directory: cd addonis-project

2. Install the project dependencies:
   ```bash
   npm install
Configure your environment variables for Firebase, API keys, and other settings.

3. Start the development server:
   ``` bash
   npm run dev
Open your web browser and access the application at http://localhost:3000.

### Usage

Browsing Addons

Visit the homepage to explore featured addons and popular categories.
Use the search bar to find addons by name and sort them by date,creator,tags and number of downloads.
Browse addon details to learn more and access download links.

Uploading Addons

Sign in or create an account to access the upload feature.
Fill out the addon details, including name, description, category, and upload the addon file.
Confirm your upload, and your addon will be published for others to discover.

Managing Your Addons

Access your user profile to view all your uploaded addons.
Edit addon details or delete addons as needed.
Keep track of user interactions and reviews on your addons.

Contributing

We welcome contributions from the community! Whether it's bug fixes, new features, or improvements to documentation, feel free to open a pull request or submit an issue.


#### Database 

![Sample Image](Team_document__2_.png)

#### Technologies Used

- Javascript

- CSS

- BOOTSTRAP v5.3.1

- HTML

- REACT v18.2.0

- REACT ROUTER v18.2.0

- REACT ROUTER DOM v6.15.0

- REACT SELECT v5.7.4

- REACT ICONS v4.11.0

- GIT

- FIREBASE

- FIREBASE HOOKS v3.10.1

- ESLINT v8.45.0

